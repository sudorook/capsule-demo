# Capsule Demo

![Build Status](https://gitlab.com/sudorook/capsule-demo/badges/master/pipeline.svg)

Sources for building
[sudorook.gitlab.io/capsule-demo](https://sudorook.gitlab.io/capsule-demo).

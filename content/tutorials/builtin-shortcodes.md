+++
title = "Built-in Shortcodes"
date = "2017-05-08T13:43:01-05:00"
tags = ["media", "images"]
categories = ["shortcodes"]
description = "Use build-in shortcodes to easily add media in Markdown pages."
+++

Hugo comes with several built-in shortcodes. Some examples of them are
displayed below. For a full list of available shortcodes, go to [the Hugo
website](https://gohugo.io/content-management/shortcodes/).

<!--more-->

## GitHub Gist
```text
{{</* gist spf13 7896402 */>}}
```
{{< gist spf13 7896402 >}}

## Twitter
```text
{{< tweet user="SanDiegoZoo" id="1453110110599868418" >}}
```
{{< tweet user="SanDiegoZoo" id="1453110110599868418" >}}

## Vimeo
```text
{{</* vimeo 146022717 */>}}
```
{{< vimeo 146022717 >}}

## Youtube
```text
{{</* youtube IS6n2Hx9Ykk */>}}
```
{{< youtube IS6n2Hx9Ykk >}}

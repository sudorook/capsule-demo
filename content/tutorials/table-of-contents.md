---
title: "Table of Contents"
date: 2017-07-29T11:01:35-04:00
categories: 
 - partials
tags: 
 - layout
---

Capsule can automatically generate a table of contents for each page. Simply
set in the front matter (toml): 

```text
toc = true
```

The title from every `#` is used to generate each item in the table. The lesser
the header is in the page hierarchy, the more it will be nested. See below.

<!--more-->

## Example 1

Consider the following header hierarchy:
```text
## Section 1 (h2)
...

### Section 2 (h3)
...

#### Section 3 (h4)
...

##### Section 4 (h5)
...

###### Section 5 (h6)
...

## Section 6 (h2)
...

### Section 7 (h3)
...
```

It yields the follow table of contents:
{{< html >}}
</div>

<aside class="menu">
  <p class="menu-label">Table of contents</p>
  <div class="menu-list">
    <nav id="TableOfContents">
      <ul>
        <li><a href="#section-1-h2">Section 1 (h2)</a>
          <ul>
            <li><a href="#section-2-h3">Section 2 (h3)</a></li>
          </ul>
        </li>
        <li><a href="#section-6-h2">Section 6 (h2)</a>
          <ul>
            <li><a href="#section-7-h3">Section 7 (h3)</a></li>
          </ul>
        </li>
      </ul>
    </nav>
  </div>
</aside>

<div class="content">
{{< /html >}}

---
title: "Downloads"
date: 2020-12-04T17:50:57-06:00
tags: 
 - text
 - bulma
categories: 
 - shortcodes
description: "Highlight Bulma-style messages within the text."
draft: yes
---

The download shortcode creates a clickable button that triggers a download from
a specified url. Syntax:

```text
{{</* download url="<url>" */>}}  
description
{{</* /download */>}}  
```

{{< download url="https://github.com/sudorook/capsule/archive/master.zip" >}}  
description
{{< /download >}}  
